Here is the test task:

Task description:
Select a random website and create a load test using an HTTP/S protocol. Load test needs to
simulate 1000 users who will visit the website in a period of 15s. You can come up with your
own performance test plan. Provide your solution using git repository.

Gather all necessary metrics during the test run, analyse and provide a report.

1. Explain the solution
2. Analyze few HTTP/S responses in details
3. Did the load test have an impact on web application response time?
4. What is the optimal application response time for modern web applications?
5. How would you define acceptable load for web applications?
